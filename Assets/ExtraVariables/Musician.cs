﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Musician : MonoBehaviour {
	[SerializeField]
	private GameObject conductorObject = null;
	public Conductor conductor { get; private set; }

	void Awake() {
		conductor = conductorObject.GetComponent<Conductor>();
	}
}
