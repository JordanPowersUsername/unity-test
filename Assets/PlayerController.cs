﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
	[SerializeField]
	private float speed = 1.0f;

	// get the rigidbody component, as this moves the player
	private Rigidbody2D rigidBody2D;
	void Awake() {
		rigidBody2D = GetComponent<Rigidbody2D>();
	}
	void FixedUpdate() {
		// get input
		float moveHorizontal = Input.GetAxisRaw("Horizontal");
		float moveVertical = Input.GetAxisRaw("Vertical");

		// create a vector from input
		Vector2 movement = new Vector2(moveHorizontal, moveVertical);
		// use this vector multiplied by speed for velocity
		rigidBody2D.velocity = movement * speed;
	}
}
