﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMovement : MonoBehaviour {

	public float speed;
	public float direction = 90.0f;

	private Rigidbody2D rigidBody2D;
	void Awake() {
		rigidBody2D = GetComponent<Rigidbody2D>();
	}

	void Start() {
		// start moving
		rigidBody2D.velocity = speed * new Vector2(Mathf.Cos(Mathf.Deg2Rad * direction), Mathf.Sin(Mathf.Deg2Rad * direction));
	}

	// destroy self when offscreen (note, if bullet is created offscreen, it won't delete until it's been on screen first)
	void OnBecameInvisible() {
		Destroy(gameObject);
	}
}
