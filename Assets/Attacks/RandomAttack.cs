﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomAttack : BaseAttack {
	[SerializeField]
	private float spreadAngle = 0.0f;
	[SerializeField]
	private byte spreadNumberOfBullets = 0;

	public override void Pattern() {
		for (byte i = 0; i < spreadNumberOfBullets; ++i) {
			// todo: derive from attack class to avoid repeated bullet creation code
			float angleBetween = spreadAngle / (spreadNumberOfBullets - 1);
			float angle = transform.rotation.eulerAngles.z +
				Random.Range(-spreadAngle / 2.0f, spreadAngle / 2.0f);
			Fire(angle);
		}
	}
}
