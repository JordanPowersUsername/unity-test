﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {
	public ushort maximumHealth;
	[HideInInspector] // health is set to maximumHealth
	public ushort health;

	private Team team;
	void Start() {
		team = GetComponent<Team>();

		health = maximumHealth;
	}

	void OnTriggerEnter2D(Collider2D collider) {
		// called when the bullet hits this
		if (collider.CompareTag("Bullet")) {
			BulletMovement bulletMovement = collider.GetComponent<BulletMovement>();
			// ignore bullets in same team
			if (collider.GetComponent<Team>().friendly == team.friendly) {
				return;
			}
			// destroy bullet
			Destroy(collider.gameObject);
			// damage this
			--health;
			// if this is damaged enough destroy this
			if (health == 0) {
				Destroy(gameObject);
			}
		}
	}
}
