﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseAttack : MonoBehaviour {
	[SerializeField]
	private GameObject bullet = null;
	[SerializeField]
	private float imprecision = 0.0f;
	[SerializeField]
	private float speed = 1.0f;

	private enum PatternOn { beat, halfBeat, quarterBeat };
	[SerializeField]
	private PatternOn patternOn;

	private HomingTarget homingTarget;
	private ColorVariables colorVariables;
	private Team team;
	private Musician musician;

	void Awake() {
		homingTarget = GetComponent<HomingTarget>();
		colorVariables = GetComponent<ColorVariables>();
		team = GetComponent<Team>();
		musician = GetComponent<Musician>();
	}

	void OnEnable() {
		switch (patternOn) {
			case PatternOn.beat:
				musician.conductor.onBeat.AddListener(Pattern);
				break;
			case PatternOn.halfBeat:
				musician.conductor.onHalfBeat.AddListener(Pattern);
				break;
			case PatternOn.quarterBeat:
				musician.conductor.onQuarterBeat.AddListener(Pattern);
				break;
		}
	}

	void OnDisable() {
		switch (patternOn) {
			case PatternOn.beat:
				musician.conductor.onBeat.RemoveListener(Pattern);
				break;
			case PatternOn.halfBeat:
				musician.conductor.onHalfBeat.RemoveListener(Pattern);
				break;
			case PatternOn.quarterBeat:
				musician.conductor.onQuarterBeat.RemoveListener(Pattern);
				break;
		}
	}

	public abstract void Pattern();

	// Fires a single bullet in a direction
	protected void Fire(float direction) {
		// apply current rotation and imprecision
		direction += transform.rotation.eulerAngles.z + Random.Range(-imprecision, imprecision);
		// create bullet
		GameObject newBullet = Instantiate(bullet, transform.position + (transform.right * 0.5f), Quaternion.identity);
		// set bullet's friendlyness
		newBullet.GetComponent<Team>().friendly = team.friendly;
		// set bullet direction and speed
		BulletMovement movement = newBullet.GetComponent<BulletMovement>();
		movement.direction = direction;
		movement.speed = speed;
		// set bullet color
		SpriteRenderer spriteRenderer = newBullet.GetComponent<SpriteRenderer>();
		spriteRenderer.color = colorVariables.primaryColor;
		// if we have a homing target, set the homing target on the bullet
		if (homingTarget) {
			bullet.GetComponent<Homing>().target = homingTarget.target;
		}
	}
}
