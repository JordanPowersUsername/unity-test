﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossController : MonoBehaviour {
	private bool isInPhase2 = false;

	Health health;
	void Start() {
		health = GetComponent<Health>();
	}

	// if not in phase2 and health is below half
	void FixedUpdate() {
		if (!isInPhase2 && health.health < health.maximumHealth / 2) {
			isInPhase2 = true;
			GetComponent<RandomAttack>().enabled = false;
			GetComponent<MoveUpAndDown>().enabled = true;
			GetComponent<Multishot>().enabled = true;
		}
	}
}
