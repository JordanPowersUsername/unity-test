﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Multishot : BaseAttack {
	[SerializeField]
	private float spreadAngle = 0.0f;
	[SerializeField]
	private byte spreadNumberOfBullets = 0;

	public override void Pattern() {
		for (byte i = 0; i < spreadNumberOfBullets; ++i) {
			Fire(Mathf.LerpAngle(
				-spreadAngle / 2.0f, spreadAngle / 2.0f,
				(float)i / (spreadNumberOfBullets - 1)
			));
		}
	}
}
