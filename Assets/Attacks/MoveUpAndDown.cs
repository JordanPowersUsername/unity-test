﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveUpAndDown : MonoBehaviour {
	private float elapsedTime = 0.0f;

	private Rigidbody2D rigidBody2D;
	void Awake() {
		rigidBody2D = GetComponent<Rigidbody2D>();
	}

	void FixedUpdate() {
		elapsedTime += Time.deltaTime;
		rigidBody2D.MovePosition(new Vector2(
			rigidBody2D.position.x,
			4.0f * Mathf.Sin(elapsedTime)
		));
	}
}
