﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Homing : MonoBehaviour {
	public GameObject target;

	public float strength;

	private BulletMovement bulletMovement;
	private Rigidbody2D rigidBody2D;
	void Awake() {
		rigidBody2D = GetComponent<Rigidbody2D>();
		bulletMovement = GetComponent<BulletMovement>();
	}

	void FixedUpdate() {
		if (target) {
			// get vector towards target
			Vector2 vectorTowardsTarget = (Vector2)target.transform.position - rigidBody2D.position;
			// normalise the vector
			vectorTowardsTarget.Normalize();
			// multiply vector by bullet speed
			vectorTowardsTarget *= bulletMovement.speed;
			// get steering vector
			Vector2 steering = vectorTowardsTarget - rigidBody2D.velocity;
			// apply steering vector
			rigidBody2D.velocity += steering * Time.deltaTime * strength / Mathf.Pow(vectorTowardsTarget.magnitude, 2.0f);
		}
	}
}
