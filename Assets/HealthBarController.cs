﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBarController : MonoBehaviour {

	[SerializeField]
	private GameObject boss = null;
	private Health health;
	private RectTransform rectTransform;
	private Vector2 maximumSize;
	private Vector2 targetSize;

	void Awake() {
		health = boss.GetComponent<Health>();
		rectTransform = GetComponent<RectTransform>();
		maximumSize = rectTransform.sizeDelta;
		// start with no width (creates animation for health appearing)
		rectTransform.sizeDelta = new Vector2(0.0f, rectTransform.sizeDelta.y);
	}

	void Update() {
		// use a linear interpolation as an animation
		const float speed = 10.0f;
		rectTransform.sizeDelta = Vector2.Lerp(rectTransform.sizeDelta, targetSize, speed * Time.deltaTime);
	}

    void FixedUpdate() {
		if (boss) {
			// resize health bar depending on boss health
			targetSize = new Vector2(maximumSize.x * health.health / health.maximumHealth, maximumSize.y);
		} else {
			// resize health bar to 0
			targetSize = new Vector2(0, maximumSize.y);
		}
	}
}
