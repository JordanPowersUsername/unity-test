﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Conductor : MonoBehaviour {
	public UnityEvent onBeat;
	public UnityEvent onHalfBeat;
	public UnityEvent onQuarterBeat;

	public float beatsPerMinute;

	private AudioSource audioSource;

	public float elapsedBeats { get; private set; } = 0.0f;

	// Awake is called as soon as the object is created
	void Awake() {
		audioSource = GetComponent<AudioSource>();
	}

	// Update is called once per frame at a fixed interval
	private int invokes = 0;
	private float interval = 1.0f / 4.0f;
	private float nextBeat = 0.0f;
	private uint audioLoops = 0;
	void FixedUpdate() {
		// find out how many beats have elapsed
		float newElapsedBeats = beatsPerMinute * (audioLoops * audioSource.clip.length + audioSource.time) / 60.0f;
		// if elapsed beat has decreased, then that means that the song has looped
		if (newElapsedBeats < elapsedBeats) {
			++audioLoops;
			newElapsedBeats = beatsPerMinute * (audioLoops * audioSource.clip.length + audioSource.time) / 60.0f; ;
		}
		elapsedBeats = newElapsedBeats;

		if (elapsedBeats > nextBeat) {

			onQuarterBeat.Invoke();
			if (invokes % 2 == 0) {
				onHalfBeat.Invoke();
			}
			if (invokes % 4 == 0) {
				onBeat.Invoke();
			}

			++invokes;
			nextBeat = invokes * interval;
		}
	}
}
